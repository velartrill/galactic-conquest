; 

(define (conv-h type name . args) (string-append
	"\t\tui::" ($ type) " " ($ name) ";\n" (if (null? args) "" (apply args-h args))
))
(define (conv-c name prop val . rest) (string-append
	"\t\t" ($ name) "." ($ prop) " = " ($ val) ";\n"
	(if (and (pair? val) (> (length val) 2))
		(apply conv-c (cdr val))
		""
	)
	(if (null? rest) "" (apply conv-c name rest))
))

(define ($ v)
	(cond 	((null? v)		"nullptr")
			((symbol? v)	(symbol->string v))
			((eq? v #f)		"false")
			((string? v)	(string-append "\"" v "\""))
			((number? v)	(number->string v))
			((pair? v)		(string-append "&" ($ (list-ref v 1))))
			((eq? v #t)		"true")
			(else "the hell have you done")
	)
)

(define (args-h prop val . rest) (string-append
	(if (pair? val) (apply conv-h val)
		"")
	(if (null? rest) "" (apply args-h rest))
))

(define-syntax with-read-file (syntax-rules ()
	((with-read-file handle file exp ...)
		(let ((handle (read (open-input-file file))))
			(begin exp ...)
		)
	)
))
(define (gen-includes include . rest) (string-append
	"#include \"" (symbol->string include) ".h\"\n"
	(if (null? rest) "" (apply gen-includes rest))
))
(define (gen-inherits inherit . rest) (string-append
	", public " (symbol->string inherit)
	(if (null? rest) "" (apply gen-inherits rest))
))
(define (dolist func list)
	(if (not(null? list)) (apply func list) "")
)

(define (gen-header name includes inherits rest)
	(string-append
		(dolist gen-includes includes)
		"namespace controllers {\n\tstruct " ($ name) " : public controller" (dolist gen-inherits inherits) " {\n\t\t"
		($ name) "();\n"
		(apply conv-h rest)
	"\t};\n}\n")
)
(define (gen-cc name includes inherits rest)
	(string-append "#include \"" ($ name) ".h\"\nnamespace controllers {\n\t"
		($ name) "::" ($ name) "() {\n"
			(apply conv-c (cdr rest))
			"\t\tview = &" ($ (list-ref rest 1)) ";\n" ;kinda hacky
		"\t};\n}"
	)
)

(with-read-file ui (list-ref (program-arguments) 1)
	(display (apply gen-header ui))
	(display (apply gen-cc ui))
)
