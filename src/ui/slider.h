#pragma once
#include "../ui.h"
#include "../color.h"

namespace ui {
	class slider: public view, public accented { public:
		long min, max, val;
		void render(rpx x, rpx y, rpx w, rpx h) const override;
		void handle_move(rpx x, rpx y, drpx dx, drpx dy, rpx ww, rpx wh) override;
		void handle_click(int btn, bool pressed) override;
		void deactivate() override;
		long hoverval;
	private:
		void drawhandle(rpx x, rpx y, rpx w, float h, long val) const;
	};
}