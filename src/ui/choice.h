#pragma once
#include "../ui.h"
#include <FTGL/ftgl.h>
#include <GLFW/glfw3.h>
namespace ui {
	class choice : public view { public:
		struct opt {
			void* id;
			const char* text;
		};
		array<opt> opts;
		rpx rowsize;
		void render(rpx x, rpx y, rpx w, rpx h) const override;
		void handle_click(int btn, bool pressed) override;
		void handle_move(rpx x, rpx y, drpx dx, drpx dy, rpx ww, rpx wh) override;
		void handle_exit() override;
		opt* cur();
		size_t idx;
		choice();
		color::colorscheme* accent;
		rpx height();
	};
}
