#include "map.h"
#include <GLFW/glfw3.h>
#include <iostream>
#include "../text.h"
#include "../window.h"
#include "../draw.h"
#include "../data/universe.h"
#include "../text.h"
#include "../ptdist.h"
#include "../strings.h"
namespace ui {
	#define STARSIZE 8
	galaxymap::galaxymap() {
		ox = oy = 0;
		scalefactor = 1;
		hoverstar = 0;
		warproutes = true;
		anim = 2;
	}
	inline float galaxymap::getscale(rpx w, rpx h) const {
		return (w < h ? w : h) * scalefactor;
	}
	void galaxymap::render(rpx x, rpx y, rpx w, rpx h) const {
		glEnable(GL_BLEND); glEnable(GL_MULTISAMPLE_ARB);
		glEnable(GL_SCISSOR_TEST); glScissor(x,h-y,w,h);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		auto& stars = data::game -> stars;
		const float scale = getscale(w,h);
		const rpx gcx = ox + x + w / 2, gcy = (y - oy) - h / 2;
		for (size_t i = 0; i < stars.size; i++) {
			const rpx cx = gcx+stars[i].x*scale, cy = gcy-stars[i].y*scale;
			const float size = stars[i].size*STARSIZE*scalefactor;
			draw::star(cx, cy, size, 30, (size_t)stars[i].type);
			size_t j;
			
		}
		if (hoverstar != 0) {
			auto& star = stars[hoverstar-1];
			const rpx cx = gcx+star.x*scale, cy = gcy-star.y*scale;
			const float size = star.size*STARSIZE*scalefactor;
			glColor3ub(255,255,255);
			const float circle_p = anim > 1 ? 1 : anim;
			const float fade_p = anim < 1 ? 0 : anim - 1;
			if (fade_p>0) {
				glColor4ub(120,120,255,120);
				draw::circle_fill(cx,cy,size*3*circle_p,30);
			}

			glColor4ub(255,255,255,140);
			draw::circle(cx, cy, size*3*circle_p, 30);

			if (warproutes && fade_p > 0) {
				for (auto& r : star.warproutes) {
					const rpx	dcx = gcx+r.dest->x*scale,
								dcy = gcy-r.dest->y*scale;
					glColor4ub(0x55,0x55,0xff,0x66*fade_p);
					glBegin(GL_LINES);
						glVertex2i(cx,cy);
						glVertex2i(dcx,dcy);
					glEnd();
				}
			}
			// render label
			glColor4ub(10,10,40,200*fade_p);
			const rpx tw = getTextW(star.name,&font::sans,14),
					tcx = cx - tw / 2;
			glBegin(GL_QUADS);
				glVertex2i(tcx-7, cy+32);
				glVertex2i(tcx+tw+7, cy+32);
				glVertex2i(tcx+tw+7, cy+8);
				glVertex2i(tcx-7, cy+8);
			glEnd();
			
			glColor4ub(130,130,255,255*fade_p);
			text(star.name,&font::sans,14,tcx,cy+15,400);
		}
		glDisable(GL_BLEND); glDisable(GL_MULTISAMPLE_ARB);
		glDisable(GL_SCISSOR_TEST);
	};
	void galaxymap::handle_move (rpx x, rpx y, drpx dx, drpx dy, rpx ww, rpx wh) {
		auto& stars = data::game -> stars;
		if (parent->activeview != this) {
			const float scale = getscale(ww,wh);
			const rpx gcx = ox + ww / 2, gcy = oy + wh / 2;
			for (size_t i = 0; i < stars.size; i++) {
				const rpx	cx = gcx + stars[i].x*scale,
							cy = gcy + stars[i].y*scale;
				if (x > cx - 10 && x < cx + 10 && y > cy - 10 && y < cy + 10) {
					if (hoverstar != i+1) {
						hoverstar = i + 1, anim = 0;
					}
					return;
				}
			}
			//otherwise:
				hoverstar = 0;
			//
		} else {
			ox+=dx;
			oy+=dy;
		}
	}
	void galaxymap::handle_click(int btn, bool pressed) {
		if (pressed == 0 && parent->activeview == this)
			window::controller -> signal(this);
	}
	void galaxymap::loop() {
		if (anim < 2) anim+=30000*window::gettime();
		if (anim > 2) anim=2;
	}

	systemmap::systemmap() {
		hoverplanet = nullptr;
	}
	float systemmap::getscale(rpx w, rpx h) const {
		auto& lastplanet = star->planets[star->planets.size-1];
		const float maxorbit = lastplanet.orbit + lastplanet.size*.2;
		const float pw = w - 100;
		return pw / maxorbit < 300 ? pw / maxorbit : 300;
	}
	void systemmap::render(rpx x, rpx y, rpx w, rpx h) const {
		const float fac = getscale(w,h);
		const rpx mid = y - h / 2;
		const rpx midx = x + w / 2;

		glEnable(GL_MULTISAMPLE_ARB);

		//draw sun

		glEnable(GL_BLEND);
		draw::glow(-300, mid, 1000, 50, star->color(), star->color(), 0x60);
		glDisable(GL_BLEND);

		for (size_t i = 0; i<star->planets.size; i++) {
			auto& p = star->planets[i];			
			color::colorscheme* scheme = data::planet::colors[(size_t)p.type];
			const rpx px = x+50 + p.orbit*fac;
			glEnable(GL_BLEND);
				//draw orbit
				if (&p == hoverplanet)
					color::apply(scheme->base.bg, 90);
				else
					color::apply(scheme->base.bg, 70);
				draw::circle(-300, mid, px+300, 365); //HIGHLY QUESTIONABLE
			glDisable(GL_BLEND);

			color::apply(scheme->base.bg);
			draw::circle_fill(px, mid, p.size*fac*.2, 50);
			glEnable(GL_BLEND);
				if (false) //pl hab
					draw::glow(px-.1*fac, mid+.1*fac, p.size*fac*.4, 20, 0xffffff, 0xffffff, 50);
				
				draw::glow(px-(p.size*0.06)*fac, mid+(p.size*0.06)*fac, p.size*fac*.15, 20, 0xffffff, 0xffffff, 0x50);
				
				if (&p == hoverplanet)
					draw::glow(px, mid, p.size*fac*.6, 20, scheme->base.fg, scheme->base.bg, 50);
			glDisable(GL_BLEND);
			//color::apply(scheme->base.fg);
			//draw::circle(px, mid, p.size*fac*.2, 20); looks ugly with border

			// yes this is crappy but I am really not sorry
		}
		glDisable(GL_MULTISAMPLE_ARB);

		// infopane
		if (hoverplanet != nullptr) {
			const rpx boxtop = y-h + 200;
			const rpx boxbtm = y-h + 50;
			const rpx labelheight = 35;
			const rpx boxw = 400;
			getTextW(hoverplanet->name, &font::sans, 14);
			color::apply(color::scheme::base.base.fg);
			glBegin(GL_QUADS);
				glVertex2i(midx-boxw/2, boxtop+labelheight);
				glVertex2i(midx+boxw/2, boxtop+labelheight);
				glVertex2i(midx+boxw/2, boxtop);
				glVertex2i(midx-boxw/2, boxtop);
			glEnd();
			color::apply(color::scheme::base.base.bg);
			glBegin(GL_QUADS);
				glVertex2i(midx-boxw/2, boxtop);
				glVertex2i(midx+boxw/2, boxtop);
				glVertex2i(midx+boxw/2, boxbtm);
				glVertex2i(midx-boxw/2, boxbtm);
			glEnd();
			color::apply(color::scheme::base.base.bg);
			text(hoverplanet->name, &font::sans, 20, midx - boxw/2, boxtop-25+labelheight, boxw, font::align::center);
			color::apply(color::scheme::base.base.fg);
			const char* type;
			// draw rest
			std::string richness = str::from((unsigned int)hoverplanet->richness);
			std::string population = str::num(hoverplanet->population);
			const char* data[] = {
				"Type", data::planet::typenames[(size_t)hoverplanet->type],
				"Richness", richness.c_str(),
				"Population", population.c_str(),
				(hoverplanet->habitable ? "Habitable": nullptr),
				nullptr
			}; // cooool
			const size_t hd[] = { 6, 0 };
			rpx ty = 0;
			table(data, hd, 13, midx - boxw/2 + 10, boxtop-25, boxw-20, 80, 6);
		}
	}
	void systemmap::handle_move (rpx x, rpx y, drpx dx, drpx dy, rpx ww, rpx wh) {
		const float fac = getscale(ww,wh);
		for (size_t i = 0; i<star->planets.size; i++) {
			auto& p = star->planets[i];			rpx mid = wh / 2;
			color::colorscheme* scheme;
			const rpx px = 50 + p.orbit*fac;
			if (ptdist(x,y, px,mid) < p.size*fac*.2) {
				hoverplanet = &p;
				return;
			}
		}
		// otherwise:
		hoverplanet = nullptr;
	}
	void systemmap::loop() {}
}
