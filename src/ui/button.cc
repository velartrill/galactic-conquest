#include "button.h"
#include <GLFW/glfw3.h>
#include "../text.h"
#include "../window.h"
namespace ui {
	button::button(container* _parent, const char* _label, const color::colorscheme* _accent) {
		parent= _parent;
		label = _label;
		accent = _accent;
		padding = 0;
	}
	void button::render(rpx x, rpx y, rpx w, rpx h) const {
		glEnable(GL_BLEND);
		glEnable(GL_MULTISAMPLE);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		bool hover = parent->hoverview == this;
		bool active = parent->activeview == this;
		color::apply(active?	accent->dark.bg :
					 hover?		accent->bright.bg:
					 accent->base.bg);
		glBegin(GL_POLYGON);
			glVertex2i(x+padding, (y-10)-padding);
			glVertex2i(x+10+padding, y-padding);
			glVertex2i(x+w-padding, y-padding);
			glVertex2i(x+w-padding, (y-h)+padding);
			glVertex2i(x+padding, (y-h)+padding);
		glEnd();

		glDisable(GL_BLEND);
		glDisable(GL_MULTISAMPLE);
		color::apply(active?	accent->dark.fg :
					 hover?		accent->bright.fg:
					 accent->base.fg);
		text(label, &font::bold, 15, x, y-(7+h/2), w, font::align::center);
	}
	void button::handle_click(int btn, bool pressed) {
		if (pressed == 0 && parent->activeview == this)
			window::controller -> signal(this);
	}
}
