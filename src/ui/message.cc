#include "message.h"
#include "../window.h"
#include "../text.h"
#include <GLFW/glfw3.h>
#include <algorithm>
namespace ui {
	message::message(container* _parent, bool _click, char* _title, char* _body, color::colorscheme* _accent) {
		parent=_parent;
		title=_title;
		body=_body;
		accent=_accent;
		click=_click;
	}
	void message::render(rpx x, rpx y, rpx w, rpx h) const {
		glEnable(GL_BLEND);
		color::apply(accent->base.bg,170);
		glBegin(GL_QUADS);
			glVertex2i(x,	y);
			glVertex2i(x+w,	y);
			glVertex2i(x+w,	y-h);
			glVertex2i(x,	y-h);
		glEnd();
		glDisable(GL_BLEND);
		color::apply(accent->bright.fg);
		text(title,&font::light,40, x+20,y-70, w-40);
		color::apply(accent->base.fg);
		text(body,&font::sans,15, x+24,y-100, w-40);

		color::apply(accent->base.fg, 80);
		if (click) {
			text((char*)"Click to continue",&font::sans,15, x, (y-h)+8, w-10, font::align::right);
		}
	}
	void message::handle_click(int btn, bool pressed) {
		if (click && pressed) window::exit();
	}
}
