#include "choice.h"
#include "../text.h"
#include <cstdio>
#include "../window.h"
namespace ui {
	void choice::render(rpx x, rpx y, rpx w, rpx h) const {
		const size_t r = opts.size*rowsize;
		if (r < h) { // draw background if there's room left
			color::apply(accent->dark.bg);
			glBegin(GL_QUADS);
				glVertex2f(x,y-r);
				glVertex2f(x+w,y-r);
				glVertex2f(x+w,y-h);
				glVertex2f(x,y-h);
			glEnd();
		}
		for (size_t i = 0; i<opts.size; i++) {
			color::apply(i+1 == idx ? accent->bright.bg : accent->base.bg);
			glBegin(GL_QUADS);
				glVertex2f(x,y-rowsize*i);
				glVertex2f(x+w,y-rowsize*i);
				glVertex2f(x+w,y-rowsize*(i+1));
				glVertex2f(x,y-rowsize*(i+1));
			glEnd();
			color::apply(i+1 == idx ? accent->bright.fg : accent->base.fg);
			opt& o = opts[i];
			text(o.text,&font::sans,15, x+10,(y-25)-i*rowsize, w-20);
		}
	};
	void choice::handle_move(rpx x, rpx y, drpx dx, drpx dy, rpx ww, rpx wh) {
		size_t p = y / rowsize;
		if (p < opts.size)	idx=p+1;  // make sure we're in range; +1
		else 				idx = 0;
	}

	void choice::handle_click(int btn, bool pressed) {
		if (idx>0 && pressed==false) {
			window::controller->signal(this);
		}
	}
	void choice::handle_exit() {
		idx=0;
	}
	choice::opt* choice::cur() {
		if (idx>0) return &opts[idx-1]; else return nullptr;
	}
	rpx choice::height(){
		return opts.size * rowsize;
	}
	choice::choice() {
		idx = 0;
		rowsize = 35;
	}
}
