#include "slider.h"
#include <GLFW/glfw3.h>
#include "../window.h"
#define handle 30
namespace ui {
	void slider::render(rpx x, rpx y, rpx w, rpx h) const {
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		color::apply(accent->base.bg);
		glBegin(GL_QUADS);
			glVertex2i(x,y);
			glVertex2i(x+w,y);
			glVertex2i(x+w,y-h);
			glVertex2i(x,y-h);
		glEnd();
		color::apply(accent->base.fg);
		drawhandle(x,y,x,h,val);
		if (parent->hoverview == this && parent->activeview == nullptr) {
			color::apply(accent->base.fg,70);
			drawhandle(x,y,x,h,hoverval);
		}
		glDisable(GL_BLEND);
	}
	void slider::handle_move(rpx x, rpx y, drpx dx, drpx dy, rpx ww, rpx wh) {
		float factor = wh / ((float)max-(float)min); // YAY CODE DUPLICATION
		hoverval = min + ((wh - (y-handle/2)) / factor);
		if (parent->activeview == this) {
			val = hoverval;
			window::controller->signal(this);
		}
	}
	void slider::handle_click(int btn, bool pressed) {
		val = hoverval;
		window::controller->signal(this);
	}
	void slider::deactivate() {

	}
	void slider::drawhandle(rpx x, rpx y, rpx w, float h, long val) const {
		float factor = h / ((float)max-(float)min);
		val -= min;
		glBegin(GL_QUADS);
			glVertex2i(x,(y-h) + val*factor);
			glVertex2i(x+w,(y-h) + val*factor);
			glVertex2i(x+w,(y-h) + val*factor - handle);
			glVertex2i(x,(y-h) + val*factor - handle);
		glEnd();
	}
}