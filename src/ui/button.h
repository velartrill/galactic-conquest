#pragma once
#include "../ui.h"
#include "../color.h"

namespace ui {
	class button: public view, accented, padded { public:
		const char* label;
		button(container*, const char* label, const color::colorscheme* accent = &color::scheme::base);
		void render(rpx x, rpx y, rpx w, rpx h) const;
		void handle_click(int btn,bool pressed);
	};
}
