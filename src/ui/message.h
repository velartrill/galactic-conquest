#pragma once
#include "../ui.h"
#include "../color.h"

namespace ui {
	class choice;
	class message : public view, accented { public:
		void render(rpx x, rpx y, rpx w, rpx h) const override;
		void handle_click(int btn, bool pressed) override;
		message(container* parent, bool click, char* title, char* message, color::colorscheme* accent = &color::scheme::base);
		char* title,* body;
		bool click;
	};
}
