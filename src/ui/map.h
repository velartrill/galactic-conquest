#pragma once
#include "../ui.h"
#include "../data/solar.h"

namespace ui {
	class galaxymap: public view { public:
		float ox, oy, scalefactor;
		galaxymap();
		void render(rpx x, rpx y, rpx w, rpx h) const override;
		inline float getscale(rpx w, rpx h) const;
		void loop() override;
		void handle_move (rpx x, rpx y, drpx dx, drpx dy, rpx ww, rpx wh) override;
		bool warproutes;
		void handle_click(int btn, bool pressed) override;
		// private:
		float anim;
		size_t hoverstar;
	};
	class systemmap: public view { public:
		systemmap();
		void render(rpx x, rpx y, rpx w, rpx h) const override;
		inline float getscale(rpx w, rpx h) const;
		void loop() override;
		void handle_move (rpx x, rpx y, drpx dx, drpx dy, rpx ww, rpx wh) override;
		data::star* star;
		data::planet* hoverplanet;

		// private:
		float anim;
	};
}