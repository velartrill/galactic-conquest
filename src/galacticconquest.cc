#include <iostream>
#include <cstdlib>
#include <ctime>

#include "err.h"
#include "window.h"
#include "controllers/welcome.h"
#include "rand.h"
int main() {
	rnd::init();
	controllers::welcome welcome;
	try {
		window::init();
		window::controller=&welcome;
		window::loop();
	} catch (const char* err) {
		std::cout<<"\e[1;31m[ERROR] \e[0;1m"<<err<<"\e[0m\n";
		return 1;
	}
}
