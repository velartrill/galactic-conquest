#include "draw.h"
#include <GLFW/glfw3.h>
#include <cmath>
#include "data/solar.h"
namespace draw {
	void circle(float x, float y, float radius, unsigned short resolution) {
		const float DEG2RAD = 3.14159/(resolution*.5);
		glBegin(GL_LINE_LOOP);
			for (int i=0; i<resolution; i++) {
				float degInRad = i*DEG2RAD;
				glVertex2f(x + cos(degInRad)*radius,y + sin(degInRad)*radius);
			}
		glEnd();
	}
	void circle_fill(float x, float y, float radius, unsigned short resolution) {
		const float DEG2RAD = 3.14159/(resolution*.5);
		glBegin(GL_TRIANGLES);
			for (int i=0; i<resolution; i++) {
				float degInRad = i*DEG2RAD;
				glVertex2f(x + cos(degInRad)*radius,y + sin(degInRad)*radius);
				glVertex2f(x, y);
				degInRad = (i+1)*DEG2RAD;
				glVertex2f(x + cos(degInRad)*radius,y + sin(degInRad)*radius);
			}
		glEnd();
	}
	void glow(float x, float y, float radius, unsigned short resolution, const color::color color1, const color::color color2, byte intensity) {
		const float DEG2RAD = 3.14159/(resolution*.5);
		glBegin(GL_TRIANGLES);
			for (int i=0; i<resolution; i++) {
				float degInRad = i*DEG2RAD;
				color::apply(color2,0);
				glVertex2f(x + cos(degInRad)*radius,y + sin(degInRad)*radius);
				color::apply(color1,intensity);
				glVertex2f(x, y);
				degInRad = (i+1)*DEG2RAD;
				color::apply(color2,0);
				glVertex2f(x + cos(degInRad)*radius,y + sin(degInRad)*radius);
			}
		glEnd();
	}
	void star(float cx, float cy, float size, unsigned short res, size_t type) {
		const color::color
			starcenter = data::star::colors[type];
		draw::glow(cx,cy,size*4, res*2, starcenter, starcenter, 100);
		if (type != (size_t)data::star::type_t::blackhole) glColor3ub(255,255,255);
		else glColor3ub(0,0,0);
		draw::circle_fill(cx,cy,size, res);
	}
}