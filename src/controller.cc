#include "controller.h"
#include <GLFW/glfw3.h>
#include "window.h"
#include <stdio.h>
namespace controllers {
	void controller::signal(void*){}
	void controller::loop(){
		view->loop();
	}
	void controller::handle_key(int key, bool pressed){
		if (key == GLFW_KEY_ESCAPE && pressed)
			window::exit();
		else
			view->handle_key(key, pressed);
	}
	void controller::drawbg(rpx w, rpx h) const {}
	controller::~controller() {}
}
