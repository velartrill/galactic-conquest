#include "grail.h"
#include <GLFW/glfw3.h>
namespace ui { namespace layout {
	void grail::render(rpx x, rpx y, rpx w, rpx h) const {
		glEnable(GL_BLEND);
		if (left.bg != nullptr) {
			color::apply(*left.bg,left.bg_alpha);
			glBegin(GL_QUADS);
				glVertex2i(x,y);
				glVertex2i(x+left.w,y);
				glVertex2i(x+left.w,y-h);
				glVertex2i(x,y-h);
			glEnd();
		}
		if (right.bg != nullptr) {
			color::apply(*right.bg,right.bg_alpha);
			glBegin(GL_QUADS);
				glVertex2i(w-right.w,y);
				glVertex2i(w,y);
				glVertex2i(w,y-h);
				glVertex2i(w-right.w,y-h);
			glEnd();
		}
		glDisable(GL_BLEND);
		if (left.view!=nullptr) left.view->render(x, y, left.w, h);
		if (right.view!=nullptr) right.view->render((x+w)-right.w, y, right.w, h);
		mainview->render(x+left.w,y,w - (left.w + right.w),h);
	}
	void grail::handle_move(rpx x, rpx y, drpx dx, drpx dy, rpx ww, rpx wh) {
		if (x<left.w) { if (left.view) hoverview = left.view, left.view->handle_move(x,y,dx,dy,left.w,wh); }
		else if (x>ww-right.w) { if (right.view) hoverview = right.view; right.view->handle_move((x+ww)-right.w, y, dx,dy, right.w, wh); }
		else hoverview = mainview; mainview->handle_move(x-left.w,y, dx, dy, ww - (left.w + right.w), wh);
	}
	void grail::loop() {
		if (left.view!=nullptr) left.view->loop();
		if (right.view!=nullptr) right.view->loop();
		mainview -> loop();
	}
}}