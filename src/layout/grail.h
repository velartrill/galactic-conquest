#pragma once
#include "../ui.h"
#include "../types.h"

namespace ui { namespace layout {
	class grail : public container, public backlit { public:
		view* mainview;
		struct {
			view* view;
			byte bg_alpha;
			rpx w;
			const color::color* bg;
		} left, right;
		void render(rpx x, rpx y, rpx w, rpx h) const override;
		void handle_move(rpx x, rpx y, drpx dx, drpx dy, rpx ww, rpx wh) override;
		void loop() override;
	};
}}