#pragma once
#include "../ui.h"
#include "../types.h"
namespace ui { namespace layout {
	class vbox : public container {
		public:
		array<view*> subviews;
		void render(rpx x, rpx y, rpx w, rpx h) const override;
		void handle_key(int key, bool pressed) override;
		void handle_move(rpx x, rpx y, drpx dx, drpx dy, rpx ww, rpx wh) override;
		void loop() override;
		rpx padding;
		rpx margin;
	};
	class vbox_l : public container {
		public:
		struct layout {
			view* view;
			float size;
		};
		array<layout> subviews;
		void render(rpx x, rpx y, rpx w, rpx h) const override;
		void handle_key(int key, bool pressed) override;
		void handle_move(rpx x, rpx y, drpx dx, drpx dy, rpx ww, rpx wh) override;
		void loop() override;
		rpx padding;
		rpx margin;
	};
}}
