#include "vcenter.h"
#include <GLFW/glfw3.h>
namespace ui { namespace layout {
	vcenter::vcenter(view* _subview, rpx _height, color::color* _bg) {
		subview=_subview;
		bg=_bg;
		height=_height;
	}
	vcenter::vcenter(){}
	void vcenter::render(rpx x, rpx y, rpx w, rpx h) const {
		drawbg(x,y,w,h);
		const rpx top = h/2 + height/2;
		subview->render(x,top,w,height);
	}
	void vcenter::handle_key(int key, bool pressed) {
		subview->handle_key(key,pressed);
	}
	void vcenter::loop() { subview->loop(); }
	void vcenter::handle_move(rpx x, rpx y, drpx dx, drpx dy, rpx ww, rpx wh) {
		if (y >= (wh/2 - height/2) && y <= (wh/2 + height/2)) {
			if (hoverview == nullptr) hoverview=subview;
			subview->handle_move(x,y-(wh/2 - height/2),dx,dy,ww,height);
		} else {
			if (hoverview!=nullptr)
				hoverview->handle_exit(),
				hoverview=nullptr;
		}
	}
}}
