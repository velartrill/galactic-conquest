#include "hbox.h"
#include <stdio.h>
namespace ui { namespace layout {
	hbox::hbox() {
		padding = 0;
	}
	void hbox::render(rpx x, rpx y, rpx w, rpx h) const {
		drawbg(x,y,w,h);
		rpx cx = x;
		rpx uw = w / subviews.size;
		for (size_t i = 0; i<subviews.size; i++) {
			subviews[i]->render(cx+padding, y-padding, uw-padding*2, h-padding*2);
			cx+=uw;
		}
	}
	void hbox::loop() {
		for (size_t i = 0; i<subviews.size; i++) {
			subviews[i]->loop();
		}
	}
	void hbox::handle_key(int key, bool pressed) {}
	void hbox::handle_move(rpx x, rpx y, drpx dx, drpx dy, rpx ww, rpx wh) {
		rpx iw = ww / subviews.size;
		size_t sv = x / iw;
		subviews[sv]->handle_move(x-iw*sv, y, dx, dy, iw, wh);
		if (hoverview!=subviews[sv]) {
			if (hoverview!=nullptr) hoverview->handle_exit();
			hoverview = subviews[sv];
		}
	}
}}
