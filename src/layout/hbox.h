#pragma once
#include "../ui.h"
#include "../types.h"
namespace ui { namespace layout {
	class hbox : public container, public backlit {
		public:
		array<view*> subviews;
		hbox();
		void render(rpx x, rpx y, rpx w, rpx h) const override;
		void handle_key(int key, bool pressed) override;
		void handle_move(rpx x, rpx y, drpx dx, drpx dy, rpx ww, rpx wh) override;
		void loop() override;
		rpx padding;
		rpx margin;
	};
}}
