#pragma once
#include "../ui.h"
#include "../color.h"

namespace ui { namespace layout {
	class vcenter : public container, public backlit { public:
		rpx height;
		void render(rpx x, rpx y, rpx w, rpx h) const override;
		view* subview;
		color::color* bg;
		vcenter(view* subview, rpx height, color::color* bg = &color::scheme::base.base.bg);
		vcenter();
		void loop();
		void handle_key(int key, bool pressed) override,
			handle_move(rpx x, rpx y, drpx dx, drpx dy, rpx ww, rpx wh) override;
	};
}}
