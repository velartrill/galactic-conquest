#inlude "../ui.h"
namespace ui { namespace layout {
	class grid {
		struct bounds { rpx ul, ur, bl, br; };
		bounds getBounds (size_t r, size_t c);
		void handle_move(rpx x, rpx y, rpx ww, rpx wh),
			handle_exit(),
			render(rpx x, rpx y, drpx dx, drpx dy, rpx w, rpx h)const=0;

	};
}}
