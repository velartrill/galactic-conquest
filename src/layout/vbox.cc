#include "vbox.h"
#include <stdio.h>
namespace ui { namespace layout {
	void vbox::render(rpx x, rpx y, rpx w, rpx h) const {
		rpx cy = y;
		rpx uh = h / subviews.size;
		for (size_t i = 0; i<subviews.size; i++) {
			subviews[i]->render(x, cy, w, uh);
			cy-=uh;
		}
	}
	void vbox::handle_key(int key, bool pressed) {}
	void vbox::handle_move(rpx x, rpx y, drpx dx, drpx dy, rpx ww, rpx wh) {
		rpx ih = wh / subviews.size;
		size_t sv = y / ih;
		hoverTo(subviews[sv], x, dx, dy, y-ih*sv, ww, ih);
	}
	void vbox::loop() {
		for (size_t i = 0; i<subviews.size; i++) {
			subviews[i]->loop();
		}
	}

	void vbox_l::render(rpx x, rpx y, rpx w, rpx h) const {
		rpx cy = y;
		for (size_t i = 0; i<subviews.size; i++) {
			rpx uh = h * subviews[i].size;
			subviews[i].view->render(x, cy, w, uh);
			cy-=uh;
		}
	}
	void vbox_l::loop() {
		for (size_t i = 0; i<subviews.size; i++) {
			subviews[i].view->loop();
		}
	}
	void vbox_l::handle_key(int key, bool pressed) {}
	void vbox_l::handle_move(rpx x, rpx y, drpx dx, drpx dy, rpx ww, rpx wh) {
		rpx ty = 0;
		for (size_t i = 0; i<subviews.size; i++) {
			rpx uh = wh * subviews[i].size;
			if (y>=ty && y<=ty+uh) {
				hoverTo(subviews[i].view, x, y-ty, dx, dy, ww, wh*subviews[i].size);
				return;
			}
			ty+=uh;
		}
	}
}}
