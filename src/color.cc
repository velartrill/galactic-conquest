#include "color.h"
#include <GLFW/glfw3.h>
#include <iostream>
namespace color {
	namespace {struct cbit {
		byte	b : 8,
				g : 8,
				r : 8;
	};}
	void apply(const color g) {
		const cbit c = *(cbit*) &g; // haaaack
		glColor3ub(c.r, c.g, c.b);
	}
	void apply(const color g, byte alpha) {
		const cbit c = *(cbit*) &g;
		glColor4ub(c.r, c.g, c.b, alpha);
	}
	namespace scheme {
		colorscheme base = {
			{0x19, 	0x7197e3},
			{0x4c, 	0x7f7bdc},
			{0x0a, 	0x001966},
		};
		colorscheme lowcontrast = {
			{0x19, 	0x7197e3},
			{0x4c, 	0x7f7bdc},
			{0x0a, 	0x001966},
		};
		colorscheme purple = {
			{0x0b0019,	0x876994},
			{0x200047,	0xc7aee5},
			{0x090014,	0x685b78},
		};

		namespace resource {
			colorscheme
				agri = {
					{150, 0x5a5aff},
					{150, 0x5a5aff},
					{150, 0x5a5aff},
				},
				mineral = {
					{0x969696, 0xffffff},
					{0x969696, 0xffffff},
					{0x969696, 0xffffff},
				},
				gas = {
					{0xafaf2c, 0xffffc8},
					{0xafaf2c, 0xffffc8},
					{0xafaf2c, 0xffffc8},
				},
				empyrium = {
					{0x289600, 0xc8ff64},
					{0x289600, 0xc8ff64},
					{0x289600, 0xc8ff64},
				},
				aetherium = {
					{0x7d2b77, 0xff96fa},
					{0x7d2b77, 0xff96fa},
					{0x7d2b77, 0xff96fa},
				};
		}
	}
}
