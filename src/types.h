#pragma once
#include <cstddef>
typedef unsigned int	rpx;	//raster pixel
typedef int				drpx;	//relative raster pixel
typedef float			vpx;	//vector pixel
typedef unsigned char	byte;
namespace data {
	typedef unsigned long	date_t;
	typedef signed long		datediff_t;
}
template <typename T> struct array {
	T* m;
	size_t size;
	array() {
		size=0;
	}
	void alloc(size_t s) {
		m = new T[s];
		size = s;
	}
	T& operator[] (size_t i) const {
		return m[i];
	}
	void free() {
		delete[] m;
	}
	void clear() {
		free();
		size=0;
	}
};
template <typename T, typename V> bool contains(V l, T item) {
	for (auto& i : l)
		if (i==item) return true;
	return false;
}
template <typename T> bool contains(array<T> l, T item) {
	for (size_t i = 0; i<l.size; i++)
		if (l[i]==item) return true;
	return false;
}
template <typename T> bool contains(array<T> l, T item, size_t* ti) {
	for (size_t i = 0; i<l.size; i++)
		if (l[i]==item) {*ti=i; return true;}
	return false;
}