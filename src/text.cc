#include "text.h"
#include <GL/gl.h>
#include <iostream>
namespace font {
	FTGLPixmapFont sans("assets/font/OpenSans-Regular.ttf");
	FTGLPixmapFont light("assets/font/OpenSans-Light.ttf");
	FTGLPixmapFont bold("assets/font/OpenSans-Bold.ttf");
	FTGLPixmapFont mono("assets/font/Inconsolata.ttf");
}
static FTSimpleLayout layout;
void text(const char* t, FTFont* font, rpx size, rpx x, rpx y, rpx len, font::align a) {
	// this is a gross hack and I'm trying not to think about it too much
	// fwiw, I'm sorry
	layout.SetFont(font);
	layout.SetAlignment(
		a == font::align::center ? FTGL::ALIGN_CENTER :
		a == font::align::right ? FTGL::ALIGN_RIGHT :
		a == font::align::justify ? FTGL::ALIGN_JUSTIFY :
		FTGL::ALIGN_LEFT
	);
	font->FaceSize(size);
	layout.SetLineLength(len);
	glRasterPos2i(x,y);
	layout.Render(t);
}
rpx getTextW(const char* t, FTFont* font, rpx size) {
	font->FaceSize(size);
	layout.SetLineLength(0);
	return layout.BBox(t).Upper().X();
}
rpx getTextH(const char* t, FTFont* font, rpx size, rpx len) {
	font->FaceSize(size);
	layout.SetLineLength(len);
	return layout.BBox(t).Upper().Y();
}
// WARNING: headers should in most cases be null-terminated
void table(const char** t, const size_t* headers, 
			rpx size, rpx x, rpx y, rpx w, rpx colw, rpx padding) {
	if (colw==0) colw=w/2;
	size_t i = 0;
	while (t[i] != nullptr) {
		if (*headers == i) {
			++ headers;
			text(t[i++], &font::bold, size, x, y, w, font::align::center);
		} else {
			text(t[i++], &font::bold, size, x, y, colw - padding, font::align::right);
			text(t[i++], &font::sans, size, x+colw+padding, y, (w-colw) - padding, font::align::justify);
		}
		y-=size+padding;
	}
}
void table(const char** t, rpx size, rpx x, rpx y, rpx w, rpx colw, rpx padding) {
	if (colw==0) colw=w/2;
	while (*t != nullptr) {
		text(*t++, &font::bold, size, x, y, colw - padding, font::align::right);
		text(*t++, &font::sans, size, x+colw+padding, y, (w-colw) - padding, font::align::justify);
		y-=size+padding;
	}
}