#include <GLFW/glfw3.h>
#include "window.h"
#include "types.h"
#include <cstdio>
namespace window {
	rpx width = 1024, height = 640;
	rpx last_mouse_x, last_mouse_y;
	const byte samples = 8;
	double time;
	controllers::controller* stack[10];	//probably won't go more than 10 deep. I'm assuming.
							//if mysterious segfaults start happening a few years down the road,
							//this is where you want to look
	controllers::controller* controller;
	controllers::controller** stackptr = stack;
	controllers::controller* queuedView = nullptr;
	GLFWwindow* window;
	bool queuedSave;
	bool queuedExit = false;
	void resize() {
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glViewport(0,0,width,height);
		glOrtho(0,width,0,height,-1,1);
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();
	}
	void handle_key(GLFWwindow* window, int key, int scancode, int action, int mods) {
		controller->handle_key(key,action==GLFW_PRESS);
	}
	void handle_click(GLFWwindow* window, int btn, int action, int mods) {
		controller->view->handle_click(btn,action==GLFW_PRESS);
	}
	void handle_move(GLFWwindow* window, double x, double y) {
		controller->view->handle_move(x,y, x - last_mouse_x, y - last_mouse_y, width, height);
		last_mouse_x = x;
		last_mouse_y = y;
	}
	void handle_resize(GLFWwindow* window, int w, int h) {
		width = w;
		height = h;
		resize();
	}
	void quit() {
		glfwSetWindowShouldClose(window,1);
	}
	double gettime() {
		return glfwGetTime()-time;
	}
	void init() {
		if (!glfwInit()) throw "welp, GLFW appears to be borked";
		glfwWindowHint(GLFW_SAMPLES, 8);
		const GLFWvidmode * mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
		width = mode->width;
		height = mode->height;
		last_mouse_x = 0; last_mouse_y = 0; // haaack
		window = glfwCreateWindow(width,height,"Galactic Conquest",glfwGetPrimaryMonitor(),nullptr);
		if (!window) throw "can't open a window; you're fucked, buddy!";
		glfwMakeContextCurrent(window);
		glDisable(GL_MULTISAMPLE);
		glfwSetKeyCallback(window,handle_key);
		glfwSetMouseButtonCallback(window,handle_click);
		glfwSetCursorPosCallback(window,handle_move);
		glfwSetWindowSizeCallback(window,handle_resize);

		resize();

		glClearColor(0,0,0,1);
	}
	void loop() {
		time=glfwGetTime();
		while(!glfwWindowShouldClose(window)) {
			if (queuedView) {
				if (queuedSave)
					*stackptr = controller, stackptr++; // save the old view on the stack
				else delete controller;

				controller = queuedView;
				queuedView = nullptr;
			} else if (queuedExit) {
				queuedExit = false;
				delete controller; 
				--stackptr;
				if (stackptr==stack) break; // if we're at the root control, end the whole program
				controller = *stackptr; // this is C after all, this kind of thing is practically tradition
				*stackptr=nullptr; // not technically necessary but it makes me feel better
			}
			controller->loop();
			glClearColor(0,0,0,1);
			glClear(GL_COLOR_BUFFER_BIT);
			controller->drawbg(width,height);
			controller->view->render(0,height,width,height);
			glfwSwapBuffers(window);
			glfwPollEvents();
			time=glfwGetTime(); // valar morghulis
		};

		glfwDestroyWindow(window);
		glfwTerminate();
	}
	void load(controllers::controller* ctrl, bool save) {
		queuedView=ctrl;
		queuedSave=save; //sigh
	}
	void exit() {
		queuedExit=true;
	}
}
