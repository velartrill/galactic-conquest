#pragma once
#include "../color.h"
namespace data {
	typedef unsigned short research_t;
	struct category {
		const char* name;
		const color::color color;
	} extern const categories[];
	struct tech {
		const char* name;
		const char* desc;
		const tech** deps;
		const size_t depc;
		const category* category;
		const research_t cost;
	} extern const techs[];
}