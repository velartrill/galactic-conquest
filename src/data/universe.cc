#include "universe.h"
#include "../rand.h"
#include "../ptdist.h"
#include <cstdlib>
#include <cstdio>
namespace data {
	const unsigned int MAX_STARS = 300;
	const unsigned int MIN_STARS = 100;
	universe::universe() {
		stars.alloc(range(MIN_STARS,MAX_STARS));
		for (size_t i = 0; i < stars.size; i++) {
			star& s = stars[i];
			s.name = star::makename();
			float oc = 0;
			s.x = (float)(range(0,200)) / 100.0f - 1.0f;
			s.y = (float)(range(0,200)) / 100.0f - 1.0f;
			s.size = (float)(range(2,10)) / 10.0f;
			byte kind = range (0, 21);
			if (kind < 9) {
				s.type=star::type_t::yellowdwarf;
			} else if (kind < 16) {
				s.type=star::type_t::redgiant;
			} else if (kind < 20) {
				s.type=star::type_t::neutronstar;
			} else {
				s.type=star::type_t::blackhole;
			}
			s.planets.alloc(range(0,14));
			for (byte i = 0; i<s.planets.size; i++) {
				oc += (float)(range(0,150)) / 100.0f + 0.5f;
				auto& p = s.planets[i];
				p.name=planet::makename();
				p.orbit=oc;
				p.size = (float)(range(0,100)) / 100.0f + 0.3f;
				p.type = (planet::type_t)range<byte>(0,(byte)planet::type_t::aetheriumworld+1); 
				p.richness = range(1,10);
				p.habitable = (p.type==planet::type_t::bioworld);
				if (s.planets[i].habitable) {
					if (range(0,3)==0) {
						p.population=range(100ul,10000ul) * p.size;
					}
				} else p.population = 0;
			}
		}
		// this algorithm is a festering piece of shit
		// but it sort of works so I'm leaving it alone
		for (size_t i = 0; i < stars.size; i++) {
			star& s = stars[i];
			byte warproutec = range(2,5);
			for (size_t i = 0; i < stars.size && warproutec > 0; i++) {
				auto& d = stars[range(0ul,stars.size)]; // fuck it
				if (ptdist(s.x, s.y, d.x, d.y) > .5) continue;
				warproute w = {&d,(char*)""}, b = {&s,(char*)""};
				s.warproutes.push_back(w);
				d.warproutes.push_back(b);
				-- warproutec;
			}
		}
	}
	universe* game;
}
