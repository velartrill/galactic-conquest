(
	(categories (construction "Construction" #xaaaaaa)
				(science "Science" #x553322)
				(politics "Politics" #x888888)
				(resources "Resources" #x553322))

	(tech
		(shipyard						construction
			"Shipyards"
			"Shipyards can build small spacefaring craft"
			() 50)

		(space-stations					construction
			"Space stations"
			"Space stations enable a permanent orbital presence" 
			() 120)

		(drydock						construction
			"Drydocks"
			"Drydocks can build much larger ships"
			(space-stations) 260)

		(habdome						construction
			"Habdome"
			"A small, cheap housing structure that can support 10 people in any environment"
			() 90)

		;; SCIENCE
		(psychology						science
			"Psychology"
			"Gain a better understanding of how people think, enabling you to better predict and manipulate them"
			() 220)

		(geology						science
			"Geology"
			"A better understanding of planetary bodies will letter you extract their resources more efficiently"
			() 540)

			(terraforming				science
				"Terraforming"
				"At great expense, make any planet capable of sustaining life."
			(geology physics) 860)

		(physics						science
			"Physics"
			"Harness the fundamental forces of the universe"
			() 160)

			(antimatter					science
				"Antimatter"
				"Use antimatter as fuel, greatly increasing the efficiency of all power sources"
				(physics) 560)

				(antimatter-drives		science
					"Antimatter Drive"
					"Harness antimatter as a propellant for your starships"
					(antimatter) 600)
		(biology						science
			"Biology"
			"Figure out where your species came from, and get a measure of control over where you're going"
			() 190)

			(neurology					science
				"neurology"
				"Unlock the brain's secrets"
				(biology) 420)

		;; RESOURCES
		(mining-gasgiant				resources
			"Gas giant mining"
			"Extract powerful gasses from gas giants"
			(space-stations) 400)

		(mining-empyrium				resources
			"Empyrium mining"
			"Exploit empyrium worlds, gaining access to the materials needed for hyperdrive construction"
			() 800)

		(mining-aetherium				resources
			"Aetherium mining"
			"Exploit aetherium worlds, gaining access to the materials needed for exotic hypertech"
			() 1400)

		;; POLITICS
		(military						politics
			"Military"
			"Conquer already-populated worlds."
			() 430)

			(improved-tactics			politics
				"Improved tactics"
				"Make your military significantly more effective"
				(military) 720)

			(technopathy				politics
				"Technopathy"
				"Use direct mind-to-mind communication to make your military 100% efficient in battle"
				(military neurology) 900)

		(state-media					politics
			"State media"
			"Maintain morale and keep citizens loyal"
			() 350)

		(nationalism					politics
			"Nationalism"
			"Nationalist propaganda can be spread on a world to increase civilian loyalty and productivity, but slow research"
			() 190)

		(oppression						politics
			"Oppression"
			"People aren't giving you what you want. Make them."
			() 280)
			(physical-torture			politics
				"Physical torture"
				"The threat of horrible suffering will improve obedience  but can foment rebellion and will negatively impact population growth"
				(oppression) 210)
			(psychological-torture		politics
				"Psychological torture"
				"Psychological torture is effective for crushing dissent, and will lessen the chance of rebellion"
				(oppression psychology) 315)
			(police-state				politics
				"Police state"
				"A police state will keep a planet's population in a state of terrified obedience, eliminating any chance of a rebellion but impacting population growth and completely impeding research"
				(oppression local-govt) 450)

			(secret-police				politics
				"Secret police"
				"A compromise between freedom and a police state, secret police can drastically lower chances of rebellion without affecting population or completely impeding research, and if a local government is present, secret police will lower chances of secession"
				(oppression delegation) 300)

			(mind-control				politics
				"Mind control"
				"While extremely expensive to maintain, mind control makes your citizens obey you without question, eliminating disobedience and maximizing efficiency and population growth, but causing extreme hatred of your regime - a mind-controlled populace will likely revolt at the first opportunity"
					(oppression neurology) 840)

		(martial-law					politics
			"Martial law"
			"Clamp down hard on a rebellious world and eliminate troublemakers at a severe cost to productivity"
			(military) 200)

		(delegation						politics
			"Delegation"
			"Leave the fiddly details of statecraft up to subordinates"
			(psychology) 100)
			(local-govt					politics
				"Local government"
				"Appoint a governor to take complete control over a planet, increasing risks of secession"
				(delegation) 300)
	)
)