#include "solar.h"
#include "../rand.h"
#include <cstring>
#include <cassert>
#include <iostream>
#include <list>
namespace data {
	const char* planet::typenames[] = {
		"Bioworld",
		"Barren",
		"Gas giant",
		"Empyrium world",
		"Aetherium world"
	};
	color::colorscheme* const planet::colors[] = {
		&color::scheme::resource::agri,
		&color::scheme::resource::mineral,
		&color::scheme::resource::gas,
		&color::scheme::resource::empyrium,
		&color::scheme::resource::aetherium,
	};
	const color::color star::colors[] = {
		0xfff385,
		0xff3333,
		0x3069f8,
		0xf000ac
	};
	color::color star::color() {
		return colors[(size_t)type];
	}
	char* star::makename() {
		enum nnt { ofgent=0, sgent, adjnt } type = (nnt)range(0,3);
		const static char* adj[] = {"Imperial", "Corrupted", "Perverted", "Adamant",
								   "Diamond", "Lightless", "Blighted", "Cursed", "Blessed", "Thunderous",
									"Teeming", "Endless", "Sightless", "Empyrean"};
		const static char* ofgen[] = {"Furnace of", "Nexus of", "Stronghold of", "Fortress of",
								   "Forge of", "Hammer of", "Refuge of", "Promise of"};
		const static char* ofgennoun[] = {"Thunder", "Wind", "Rain", "Shadow", "Light", "Faith", "Perversion",
									"Corruption", "Strength", "Hammer", "Fall", "Blight", "Paradise",
									"Refuge", "Virtue", "Salvation", "Eternity", "Absolution", "Glory",
									"Terror", "Power", "Might", "Gods", "Demons", "Passage"};
		const static char* sgen[] = {"Abbadon's", "Bael's",  "Hammer's", "Heretic's", "Fire's", "Rain's", "Hell's",
									"Heaven's", "Terror's", "Fear's"};
		const static char* sgennoun[] = {"Thunder", "Wind", "Glory", "Rain", "Shadow", "Light", "Faith", "Perversion",
									 "Corruption", "Strength", "Hammer", "Fall", "Reach", "Redoubt", "Furnace", "Nexus", 
									 "Core", "Blight", "Paradise", "Crest", "Pinnacle", "Refuge", "Gate",
									 "Virtue", "Might", "Purge", "Pain"};
		const char** gen,** noun;
		byte genc, nounc;
		if (type==ofgent) {
			gen = ofgen;
			genc = sizeof(ofgen)/sizeof(char*);
			noun = ofgennoun;
			nounc = sizeof(ofgennoun)/sizeof(char*);
		} else if (type==sgent) {
			gen = sgen;
			genc = sizeof(sgen)/sizeof(char*);
			noun = sgennoun;
			nounc = sizeof(sgennoun)/sizeof(char*);
		} else {
			gen = nullptr;
			noun = ofgennoun;
			nounc = sizeof(ofgennoun)/sizeof(char*);
		}
		// better method might be malloc()ing the maximum size then shortening with
		// realloc; depends on how fast realloc() is
		const char* adi =
			range(0,3) != 0 ?
				adj[range<size_t>(0,sizeof(adj)/sizeof(char*))]
			: nullptr;
		byte geni;
		if (gen != nullptr) geni = range<byte>(0,genc);
		const byte ni = range<size_t>(0,nounc);
		const byte gsize = gen == nullptr ? 0 : (strlen(gen[geni]) + 1);
		size_t nsize = 1;
		if (adi!=nullptr) nsize += strlen(adi)+1;
		nsize += strlen(noun[ni])+gsize;
		char* name = new char[nsize];
		name[0]=0;
		if (gen != nullptr) {
			strcat(name,gen[geni]);
			strcat(name," ");
		}
		if (adi!=nullptr) {
			strcat(name,adi);
			strcat(name," ");
		}
		strcat(name,noun[ni]);
		return name;
	}
	#define ssize(a) (sizeof(a)/sizeof(decltype(a[0]))-1)
	#define randletter(a) (a[range<size_t>(0,ssize(a))]) //leaving out the +1 so the trailing \0 on strings
												// won't be included
	char* planet::makename() {
		// as a linguist this makes me cry
		const char consonants[] = "ptkbdgfsxvzhjlkmnpqrwy";
		const char vowels[] = "aeiou";
		byte len = range(3,10);
		char* name = new char[len + 1];
		name[len] = 0;
		bool consonant = range(0,2);
		bool coda = false; // keep word-final double codas away - haaack
		for (byte i = 0; i<len; i++) {
			if (consonant && !(i==len-1 && coda)) name[i] = randletter(consonants);
			else {
				name[i] = randletter(vowels);
				if (i<len-1 && range(0,2)==0)
					name[++i] = randletter(consonants),
					coda=true;
				else coda= false;
			}
			consonant=!consonant;
		}
		name[0] -= 0x20;
		return name;
	}
	/*
		using std::list;
		bool calcroute(star* src, star* dest, list<star*>& state, array<star*>& solution, size_t depth) {
			state.push_front(src);
			for (auto& w : src->warproutes) {
				if(w.dest==dest) {
					solution.alloc(depth);
					return true;
				}
			}
			for (auto& w : src->warproutes) {
				if (contains(state,w.dest))
					continue;
				if (calcroute(w.dest, dest, state, solution, depth+1)) {
					solution[depth] = w.dest;
					return true;
				}
			}
			return false;
		}
	

	bool star::routeto(star* dest, array<star*>& solution) {
		if (dest==this) return false;
		std::list<star*> state;
		if (calcroute(this, dest, state, solution, 0)) {
			solution[0] = this;
			return true;
		} else {
			return false;
		}
	}*/
}
