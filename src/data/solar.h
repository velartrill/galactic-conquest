#pragma once
#include "../types.h"
#include "polity.h"
#include <list>
#include "../color.h"
namespace data {
	struct planet {
		const char* name;
		byte richness;
		float size;
		float orbit;
		long population;
		bool habitable;
		polity* owner;
		enum class type_t {
			bioworld = 0,	// inhabitable, can produce food
			mineral,	// uninhabitable, can be mined for minerals, terraformed
			gasgiant,	// uninhabitable, can be mined for gases
			empyriumworld, // uninhabitable, can be mined for empyrium, terraformed
			aetheriumworld, // uninhabitable, can be mined for aetherium
		} type;
		static const char* typenames[];
		static color::colorscheme* const colors[];
		static char* makename();
		bool terraforming;
		unsigned int terraform_time;
	};
	struct star;
	struct warproute {
		star* dest;
		char* name;
		// other metadata? stability, etc?
		// give them a terrible chaos-y name?
	};
	struct star {
		const char* name;
		enum class type_t { yellowdwarf =0, redgiant, neutronstar, blackhole } type;
		array<planet> planets;
		std::list<warproute> warproutes;
		float x;
		float y;
		float size;
		static char* makename();
		static const color::color colors[];
		color::color color();
		//bool routeto(star*, array<star*>& solution);
	};
}
