#pragma once
#include "types.h"
#include "color.h"
namespace draw {
	void circle(float x, float y, float radius, unsigned short resolution);
	void circle_fill(float x, float y, float radius, unsigned short resolution);
	void glow(float x, float y, float radius, unsigned short resolution, const color::color center, const color::color edge, byte intensity = 255);
	void star(float cx, float cy, float size, unsigned short resolution = 9, size_t type = 0);
}