#pragma once
#include "types.h"
#include "controller.h"
namespace window {
	extern rpx width, height;
	extern double time;
	extern controllers::controller* controller;
	double gettime();
	void
		init(),
		loop(),
		load(controllers::controller*, bool save=true),
		exit(),
		quit();
}
