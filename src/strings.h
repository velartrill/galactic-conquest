#pragma once
#include <string>
#include <sstream>
namespace str {
	template <typename T> std::string from(T n) {
		std::ostringstream ss;
		ss<<n;
		return ss.str();
	}
	template <typename T> T to(std::string n) {
		std::istringstream ss(n);
		T retval;
		ss >> retval;
		return retval;
	}
	template <typename T> std::string num(T v) {
		std::string in = from(v);
		if (in.size()<=3) return in;
		std::string out = "";
		unsigned char count = 0;
		for (int i = in.size()-1; i>=0; i--) {
			out=in[i]+out;
			if (count==2) {
				count=0;
				if (i!=0) out=','+out;
			} else count++;
		}
		return out;
	}

}