#include <cstdlib>
#include "types.h"
#include <random>
#include <iostream>
namespace rnd {
	extern std::mt19937 eng;
	void init();
}
template <typename N> N range(N min, N max) {
	std::uniform_int_distribution<N> uniform(min,max-1);
	return uniform(rnd::eng);;
}
