#pragma once
#include "types.h"
#include <cstdint>
namespace color {
	/*struct color {
		byte r, g, b;
	};*/
	typedef uint32_t color;
	void apply(color);
	void apply(color, byte alpha);
	struct colorpair {
		color bg, fg;
	};
	struct colorscheme {
		colorpair base;
		colorpair bright;
		colorpair dark;
	};
	namespace scheme {
		extern colorscheme
			base,
			lowcontrast,
			purple;
		//	urgent,
		//	dim;

		namespace resource {
			extern colorscheme
				agri, mineral,
				gas, empyrium,
				aetherium;
		}
	}
}
