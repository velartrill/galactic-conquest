#pragma once
#include <FTGL/ftgl.h>
#include "types.h"
namespace font {
	extern FTGLPixmapFont sans, light, bold, mono;
	enum class align {left, right, center, justify};
}
void text(const char* t, FTFont* font, rpx size, rpx x, rpx y, rpx len, font::align a = font::align::left);
rpx getTextW(const char* t, FTFont* font, rpx size);
rpx getTextH(const char* t, FTFont* font, rpx size, rpx len);
void table(const char** t, const size_t* headers, rpx size, rpx x, rpx y, rpx w, rpx colw = 0, rpx padding = 5);
void table(const char** t, rpx size, rpx x, rpx y, rpx w, rpx colw = 0, rpx padding = 5);