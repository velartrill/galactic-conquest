#pragma once
#include "ui.h"
namespace controllers {
class controller {public:
	ui::view* view;
	virtual void signal(void*);
	virtual void loop();
	virtual void drawbg(rpx w, rpx h) const;
	virtual void handle_key(int, bool pressed);
	virtual ~controller();
};
}
