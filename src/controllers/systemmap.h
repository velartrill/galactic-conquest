#include "../ui.h"
#include "../controller.h"
#include "../ui/map.h"
#include "../data/solar.h"

namespace controllers {
	class systemmap: public controller { public:
		ui::systemmap map;
		data::star* star;
		systemmap(data::star*);
	};
}