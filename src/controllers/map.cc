#include "map.h"
#include "../data/universe.h"
#include "../window.h"
#include "systemmap.h"
namespace controllers {
	map::map() {
		const static color::color vcol = 0xffffbe;
		main.left.view=nullptr;
			main.left.bg=&color::scheme::base.base.bg;
			main.left.bg_alpha=255;
			main.left.w=200;

		main.right.view=&mapzoom;
			main.right.bg=&color::scheme::base.bright.bg;
			main.right.bg_alpha=255;
			main.right.w=40;

			mapzoom.parent = &main;
			mapzoom.accent = &color::scheme::purple;
			mapzoom.min = 30;
			mapzoom.max = 100;
			mapzoom.val = 100;

		main.mainview=&gmap;
		gmap.parent=&main;
		view=&main;
	};
	void map::signal(void* v) {
		if (v==&mapzoom) {
			gmap.scalefactor = (float)mapzoom.val / 100.0f;
		} else if (v==&gmap) {
			if (gmap.hoverstar != 0) {
				window::load(new controllers::systemmap(&data::game->stars[gmap.hoverstar-1])); // ew
			}
		}
	}
	void map::handle_key(int key, bool pressed) {
		view->handle_key(key,pressed);
	}
}
