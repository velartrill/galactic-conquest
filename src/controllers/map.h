#pragma once
#include "../color.h"
#include "../ui.h"
#include "../ui/choice.h"
#include "../ui/button.h"
#include "../ui/message.h"
#include "../ui/slider.h"
#include "../ui/map.h"
#include "../layout/grail.h"
#include "../controller.h"

namespace controllers {
	class map : public controller { public:
		const static color::color
			left_color,
			right_color;
		ui::galaxymap gmap;
		ui::layout::grail main;
		ui::slider mapzoom;
		map();
		void signal(void*) override;
		void handle_key(int,bool) override;
	};
}