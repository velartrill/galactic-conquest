#pragma once
#include "../ui.h"
#include "../ui/choice.h"
#include "../ui/button.h"
#include "../ui/message.h"
#include "../layout/hbox.h"
#include "../layout/vbox.h"
#include "../layout/vcenter.h"
#include "../controller.h"

namespace controllers {
	class welcome : public controller {
		public: // why the fuck do I have to do this
		struct pt { float x, y, size; };
		pt starfield[90];
		welcome();
		ui::layout::vcenter center;
			ui::layout::vbox_l rows;
					ui::message main_text;
				ui::layout::hbox btns;
					ui::button btn[4];
					ui::view* btna[4];
		ui::layout::vbox_l::layout rowa[2];
		void drawbg(rpx w, rpx h) const override;
		void loop() override;
		void signal(void*) override;	
	};
}
