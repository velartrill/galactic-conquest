#include "welcome.h"
#include "../draw.h"
#include "../window.h"
#include "map.h"
#include "../data/universe.h"
namespace controllers {
	welcome::welcome() :
		main_text (nullptr, false, (char*)"Welcome",(char*)"Soon you will all perish screaming.\nWon't that be nice?"),
		btn{ui::button(&btns, "New Game", &color::scheme::purple),
			ui::button(&btns, "Load Game", &color::scheme::purple),
			ui::button(&btns, "About", &color::scheme::purple),
			ui::button(&btns, "Quit", &color::scheme::purple)},
		btna{&btn[0],&btn[1],&btn[2],&btn[3]}
	{
		btns.subviews.m = btna;
		btns.subviews.size = sizeof(btna)/sizeof(ui::view*);
		btns.padding = 5;

		rowa[0].view = &main_text;
		rowa[0].size = .8;
		rowa[1].view = &btns;
		rowa[1].size = .2;
		rows.subviews.m=rowa;
		rows.subviews.size=2;

		center.subview = &rows;
		center.height = 250;
		center.bg=nullptr;

		view=&center;

		for (byte i=0; i < sizeof(starfield)/sizeof(pt); ++i) {
			starfield[i].x=(float)(rand()%2000)/1000.0f - 1.0f;
			starfield[i].y=(float)(rand()%2000)/1000.0f - 1.0f;
			starfield[i].size=(float)(rand()%1000)/1000.0f;
		}
	}

	void welcome::drawbg(rpx w, rpx h) const {
		glEnable(GL_BLEND); glEnable(GL_MULTISAMPLE_ARB);
		for (byte i=0; i < sizeof(starfield)/sizeof(pt); ++i) {
			rpx cx = w/2 + ((float)starfield[i].x)*w,
				cy = h/2 + ((float)starfield[i].y)*h,
				size = 10.0f * starfield[i].size;
			draw::star(cx,cy,size);
		}
		glDisable(GL_MULTISAMPLE_ARB); glDisable(GL_BLEND);
	}
	
	void welcome::loop() {
		for (byte i=0; i < sizeof(starfield)/sizeof(pt); ++i) {
			starfield[i].x+=starfield[i].size*window::gettime()*1000; // ADD TIMING CODE
			if (starfield[i].x>1) {
				starfield[i].x=-1;
				starfield[i].y=(float)(rand()%2000)/1000.0f - 1.0f;
					// <del>I don't understand why it's .5 instead of 1 but w/e</del>
					// edit: b/c i can't math. fixed.
				starfield[i].size=(float)(rand()%1000)/1000.0f;
			}
		}
	}
	void welcome::signal(void* w) {
		if (w == &btn[3]) {
			window::quit();
		} else if (w==&btn[0]) {
			data::game = new data::universe;
			window::load(new map);
		}
	}
}
