#include "ui.h"
#include <GLFW/glfw3.h>
namespace ui {
	view::~view() {};
	void view::handle_key(int key, bool pressed) {}
	void view::handle_move(rpx x, rpx y, drpx dx, drpx dy, rpx ww, rpx wh) {}
	void view::handle_click(int btn, bool pressed) {}
	void view::handle_exit() {}
	void view::loop(){};
	void view::deactivate() {};
	container::container() {
		hoverview = nullptr;
		activeview = nullptr;
		focusview = nullptr;
	}
	void container::focusOn(view* v) {
		focusview = v;
		if (parent != nullptr)
			parent -> focusOn(this);
	}
	void container::blur() {
		focusview = nullptr;
		if (parent != nullptr)
			parent -> blur();
	}
	void container::handle_exit() {
		if (hoverview!=nullptr)
			hoverview->handle_exit(),
			hoverview = nullptr;
	}
	void container::hoverTo(view* v, rpx x, rpx y, drpx dx, drpx dy, rpx w, rpx h) {
		v->handle_move(x,y,dx,dy,w,h);
		if (hoverview!=v) {
			if (hoverview!=nullptr) hoverview->handle_exit();
			hoverview = v;
		}
	}
	void container::handle_click(int btn, bool pressed) {
		if (hoverview!=nullptr) {
			hoverview->handle_click(btn,pressed);
			if (pressed) {
				if (activeview == nullptr) {
					activeview = hoverview;
				}
			}
		}
		if (!pressed && activeview != nullptr) {
			deactivate();
		}
	}
	void container::deactivate() {
		view* v = activeview; // hack to ensure tail optimization
		activeview = nullptr;
		if (v) v -> deactivate();
	}

	void backlit::drawbg(rpx x, rpx y, rpx w, rpx h) const {
		if (bg != nullptr) {
			color::apply(*bg);
			glBegin(GL_QUADS);
				glVertex2f(x,y);
				glVertex2f(x+w,y);
				glVertex2f(x+w,y-h);
				glVertex2f(x,y-h);
			glEnd();
		}
	}
	backlit::backlit() {
		bg=nullptr;
	}
}
