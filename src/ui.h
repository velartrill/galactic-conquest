#pragma once
#include "types.h"
#include "color.h"
namespace ui {
	enum class state {
		off, hover, down
	};
	class container;
	class view { public:
		virtual void
			render(rpx x, rpx y, rpx w, rpx h) const =0,
			handle_key(int key, bool pressed),
			handle_move(rpx x, rpx y, drpx dx, drpx dy, rpx ww, rpx wh),
			handle_click(int btn, bool pressed),
			handle_exit();
		virtual ~view();
		virtual void loop();
		virtual void deactivate(); //christ this is suck a hack
		container* parent;
	};
	class accented { public:
		const color::colorscheme* accent;
	};
	class padded { public:
		rpx padding;
	};
	class backlit { public:
		const color::color* bg;
		void drawbg(rpx x, rpx y, rpx w, rpx h) const;
		backlit();
	};
	class container : public view { public:
		container();
		view* hoverview;
		view* activeview;
		view* focusview;
		void focusOn(view*);
		void blur();
		void hoverTo(view*, rpx x, rpx y, drpx dx, drpx dy, rpx w, rpx h);
		virtual void handle_click(int btn, bool pressed);
		virtual void handle_exit();
		void deactivate() override;
	};
}
