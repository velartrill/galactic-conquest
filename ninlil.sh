#/usr/bin/env bash

# ninlil build script copyright 2014 alexis summer hale.
# this script is released under the terms of the 3-clause
# BSD license.

## config options
	cc="clang++ -g" # compiler invocation
        FREETYPE=`freetype-config --cflags`
        FREETYPELD=`freetype-config --libs`
	cargs="-std=c++11 -Xclang -fcolor-diagnostics $FREETYPE" # arguments for compilation phase
	linkargs="-lglfw -lGL $FREETYPELD -lftgl" # arguments for the linking phase
	project=$(basename $(pwd)) # name of the executable
	simul=false # build everything in parallel. disables error-checking
	windowed=true # pipe script output to less
	errstop=true # stop all compilation as soon as any error is encountered.
	srcdir="src" # source directory
	exts="*.cc" # source extensions to compile
##

ninlil(){

basedir=`pwd`
say() {
	echo -e " \e[7;3${3};1m${1}\e[0;3${3}m $2\e[0m"
}
del() {
	if [ -e $1 ]; then
		say ' RM ' "$1" 3
		rm "$1"
	fi
}
fmerr() {
	echo "$1" | sed 's/^/    \x1b[31m:\x1b[0m /'
}
fail() {
	say ' ER ' 'build failed.' 1
	exit 1
}
enter(){
	cd $1
	wd=$(pwd)
	f=${wd#$basedir}
	say ' CD ' "$f" 4 
}
compile(){
	say ' CC ' "$1" 5
	if $simul; then
		$cc $cargs -c "$1" -o "$2"
	else
		buf=$($cc $cargs -c "$1" -o "$2" 2>&1)
		if [ ! $? -eq 0 ]; then
			say ' ER ' 'compile error' 1
			fmerr "$buf"
			del "$2"
			if $errstop; then fail; fi
		elif [ "$buf" != "" ]; then
			say 'WARN' 'compile warning:'
			fmerr "$buf"
		fi
	fi
}
clean() {
	del "$2"
}
descend(){
	if ls $exts &> /dev/null; then
		for f in $exts; do
			ofile=$(basename $f)
			ofile=${ofile%.*}".o" # change extension
			if [ ! -e "$ofile" ] || [ $f -nt $ofile ]; then
				compile "$f" "$ofile"
			fi
			link="$link "`pwd`"/$ofile"
		done
	fi
	for d in */; do
		if [ -d "$d" ]; then
			enter "$d"
				descend
			cd ..
		fi
	done
}
descend_clean(){
	if ls *.o &> /dev/null; then
		for f in *.o; do
			del $f
		done
	fi
	for d in *; do
		if [ -d "$d" ]; then
			enter "$d"
				descend_clean
			cd ..
		fi
	done
}

link=''

if [ ! -e "src/data/tech.cc" ] || [ "src/data/tech.t" -nt "src/data/tech.cc" ]; then
	say ' PR ' 'tech defs' 3
	guile parsetech.scm src/data/tech.t > src/data/tech.cc
fi

enter "$srcdir"
	if [ "$op" = "clean" ]; then
		descend_clean
	else
		descend
	fi
	wait
cd ..
if [ "$op" != "clean" ]; then
	say 'LINK' "$project" 2
	buf="$($cc $link $linkargs -o $project 2>&1)"
	if [ ! $? -eq 0 ]; then
		say ' ER ' 'link error:' 1
		fmerr "$buf"
		del $project
		fail
	fi
else
	del $project
fi

}
if [ "$1" = "clean" ]; then
	op='clean'
else
	op='compile'
fi

if $windowed; then
	ninlil | less -R
else
	ninlil
fi
