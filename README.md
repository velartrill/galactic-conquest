# galactic-conquest
galactic-conquest is a game of galactic conquest, in which you conquer 
the galaxy. ostensibly. that's the idea, anyway. we'll see if it ever 
gets there. right now you can mostly just sort of poke around and make 
the engine crash in interesting ways.

## building
install all dependencies and execute `./ninlil.sh` from the root of the 
game directory to build the game, creating the `galacticconquest` 
binary. `./ninlil.sh clean` removes object files.

## running
galactic-conquest must be run from the directory where its assets folder 
is located. there is no procedure for installation yet.

## dependencies
* FTGL
* GLFW3
* `bash`

additionally, several Scheme scripts are used to generate C++ code from 
data files. running these scripts requires GNU Guile, but it is not 
strictly required for compilation. the `ninlil` make script is written 
in bash, but will also run under `zsh`.

galactic-conquest compiles with clang++, but other C++11-compliant 
compilers should be usable as well.

galactic-conquest is only confirmed to build on arch linux.

## license
galactic-conquest is released under the GPLv3. copyright 2014 Alexis 
Hale.